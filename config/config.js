const config = {
  app: {
    port: 1234
  },
  db: {
    host: 'localhost',
    port: 22222,
    name: 'db'
  }
};

module.exports = config;