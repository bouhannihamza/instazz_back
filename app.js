

var express = require("express");
var config = require("./config/config");
var Routes = require("./components");



const { db: { host, port, name } } = config;

/*
const mongoose = require('mongoose');
let dev_db_url = 'mongodb://instazz:instazz0@ds111072.mlab.com:11072/instazz';
const mongoDB = process.env.MONGODB_URI || dev_db_url;
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
*/
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'erreur de  connection:'));
  const app = express();
  app.use("/", Routes);

  const PORT = config.app.port;
  app.listen(PORT, () => {
    console.log(`server running on port ${PORT}`);
  });

