import express from "express";
import multer from "multer";
import post from "./post/routes";
import   getPic from "./storage";
import  postPic from "./storage";

const routes = express.Router();

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "upload/");
  },
  filename: function(req, file, cb) {
    cb(null, file.originalname);
  }
});

const upload = multer({ storage: storage });
const type = upload.single("photo");

routes.use("/api/", post);
routes.get("/isi", getPic);
routes.post("/isi", type, postPic);

export default routes;
