import Post from "./model";

module.exports = {
  
  list: async function() {
    let result = await Post.find({});
    return result;
  },
  
  createPost: async function(post) {
    if (post) {
      return Post.create(post);
    }
  }
  
};
