import express from "express";
import Post from "./controller";


const routes = express.Router();


routes.route("/posts").post(Post.post);


routes.route("/posts").get(Post.list);



export default routes;
