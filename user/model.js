import mongoose from "mongoose";
import Post from "./../post/model";

const Schema = mongoose.Schema;

var UserSchema = new Schema({
  name: String,
  username: {
    type: String,
    index: true,
    unique: true
  },
  password: String,
  email: String,
  description: String,
  posts: [Post.schema]
});

UserSchema.index({ name: 1});
let User = mongoose.model("User", UserSchema);
User.createIndexes();

export default User;