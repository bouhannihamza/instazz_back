import express from 'express';
import * as User from "./controller";

const routes = express.Router();

routes.route("/profile").get(User.profile);

routes.route("/user").post(User.post);

routes.route("/user/:id/posts").get(User.posts);

routes.route("/users").get(User.list);

export default routes;
