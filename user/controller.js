import * as services from "./services";
import {postsByUser} from "./../post/services";


export function login(req, res){
  services.createUser(req.body).then(
    user => res.status(200).json(user),
    err => {
      res.status(500).send("error");
      return;
    }
  );
};

export async function profile(req, res){
  res.status(200).send({
      user: req.user
  });
};

export function post(req, res){
    if (!req.body.name ) {
        return res.status(400).send({message: "information name is required !!"});
    }
    services.createUser(req.body).then(
      user => res.status(200).json(user),
      err => {
        res.status(500).send("error");
        return;
      }
    );
};

export async function update(req, res){
  const user = await services.updateUser(req.params.id, req.body);
  res.status(200).send({
    user: user
  });
};

export async function posts(req, res){
  const list = await postsByUser(req.params.id)
  res.status(200).send({
      posts: list
  });
};

export async function list(req, res){
    const list  = await services.listByPage(req.query.page || 1, req.query.per_page || 10)
    res.status(200).send({
        users: list
    });
};











  
